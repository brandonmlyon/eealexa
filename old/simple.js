
// SETTING THINGS UP

// Unirest is a lightweight http request library
// http://unirest.io/nodejs.html

// Load the Unirest module
var unirest = require('unirest');
// Test to see if a required module is present
function moduleAvailable(unirest) {
    try {
        require.resolve(unirest);
        return true;
    } catch(e){}
    return false;
}

var destinationUrl = ''; // appi url
var destinationHeaders = ''; // api authentication
var destinationInput = ''; // api func request

// Log the reults of the request
var returnCallback = function(results)
{
  console.log(results);
  return results;
};

// Perform the API request
returnResults = function returnResults(destinationInput, returnCallback) {
  unirest.post(destinationUrl)
  .headers(destinationHeaders)
  .send(destinationInput)
  .end(
    function (response)
    {
      // WHY DOES THIS LOG NEVER HAPPEN?!
      console.log("unirest '.end' function was triggered");
      /**/
      var results = "no results found"; // placeholder
      returnCallback(results);
    }
  );
  // BUT THIS LOG DOES HAPPEN THEREFORE WE KNOW THAT RETURNRESULTS HAPPENS
  console.log("returnSearchResults finished, awaiting results from unirest.post.");
};

// This is what happens when the AWS Lambda is run.
function handleTestRequest(intent, session, callback)
{
  // Check to see if Unirest is loaded.
  if (moduleAvailable('unirest')) {
      console.log("unirest found");
  }

  // This runs returnResults
  var finalResponse = returnResults(destinationInput, returnCallback);
  callback(
    session.attributes,
    buildSpeechletResponseWithoutCard(finalResponse, "", "true")
  );

}

// This builds json to return to AWS.
function buildSpeechletResponseWithoutCard(output, repromptText, shouldEndSession) {
  return {
    outputSpeech:
    {
      type: "PlainText",
      text: output // The final API response goes here but currently ends up blank.
    },
    reprompt:
    {
        outputSpeech:
        {
          type: "PlainText",
          text: repromptText
        }
    },
    shouldEndSession: shouldEndSession
  };
}
