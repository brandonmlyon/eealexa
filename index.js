

/*
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

20170405: Brandon says:

I'm almost there! I got the Lambda to return the contents of the API to Alexa.

Left off at:
* Success log should not be null. It should have the finalResults.

After that:
* Need to retrofit the other functions and not just handleTestRequest.

Don't forget to:
* Strip HTML out of the result?

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
*/


// unirest is a lightweight http request library
// http://unirest.io/nodejs.html
var unirest = require('unirest');
// http://stackoverflow.com/a/33067955
function moduleAvailable(unirest) {
    try {
        require.resolve(unirest);
        return true;
    } catch(e){}
    return false;
}
var destinationUrl = 'http://www.experts-exchange.com/jsp/mobileAPI.jsp';
var destinationHeaders =
{
  'Accept': 'vnd.experts-exchange.api+json;version=X',
  'x-ee-cmssiteid': '2',
  /* !IMPORTANT TODO: REMOVE THIS EE-SLI KEY */
  'x-ee-sli': '4825914:2727d0e660193c65036accadb64b88be778d88ac',
  'User-Agent': 'eeAlexa'
};
/* SIMPLE SEARCH */
/*var destinationInput =
{
    'apiFunc': 'Search:simple',
    'searchTerms': 'mongo typo'
};*/
/* QUESTION GET */
var destinationInput =
{
  'apiFunc': 'Question:get',
  'qid': '28732722',
  'allComments': 'false'
};
/* ARTICLE GET */
/*var destinationInput =
{
  'apiFunc': 'Article:get',
  'articleID': '19121',
  'allComments': 'false'
};*/
// Process the Search results from EE
var returnCallback = function(results)
{
  /*console.log(results);*/
  return results;
};

/*
Based on a template from Brian Donohue https://github.com/Donohue/alexa
*/

'use strict';

// Route the incoming request based on type (LaunchRequest, IntentRequest,
// etc.) The JSON body of the request is provided in the event parameter.
exports.handler = function (event, context) {

  try
  {
    /*console.log("event.session.application.applicationId=" + event.session.application.applicationId);*/

    /*
    Uncomment this if statement and populate with your skill's application ID to
    prevent someone else from configuring a skill that sends requests to this function.
    */
    if (event.session.application.applicationId !== "amzn1.ask.skill.03c367a6-9104-4ceb-8f99-ea0dbdfc9a7e")
    {
       context.fail("Invalid Application ID");
    }

    if (event.session.new)
    {
        onSessionStarted({requestId: event.request.requestId}, event.session);
    }

    if (event.request.type === "LaunchRequest") {
        onLaunch(
          event.request,
          event.session,
          function callback(sessionAttributes, speechletResponse)
          {
            context.succeed(buildResponse(sessionAttributes, speechletResponse));
          }
        );

    } else if (event.request.type === "IntentRequest") {
      onIntent(
        event.request,
        event.session,
        function callback(sessionAttributes, speechletResponse)
        {
          context.succeed(buildResponse(sessionAttributes, speechletResponse));
        }
      );

    } else if (event.request.type === "SessionEndedRequest") {
        onSessionEnded(event.request, event.session);
        context.succeed();
    }

  } catch (e) {
      context.fail("Exception: " + e);
  }

}

/*
Called when the session starts.
*/
function onSessionStarted(sessionStartedRequest, session)
{
  /*console.log("onSessionStarted requestId=" + sessionStartedRequest.requestId
    + ", sessionId=" + session.sessionId);*/

  // add any session init logic here

}

/*
Called when the user invokes the skill without specifying what they want.
*/
function onLaunch(launchRequest, session, callback)
{
  /*console.log("onLaunch requestId=" + launchRequest.requestId
    + ", sessionId=" + session.sessionId);*/

  var cardTitle = "Hello, World!";
  var speechOutput = "You can tell Hello, World! to say Hello, World!";

  callback(
    session.attributes,
    buildSpeechletResponse(cardTitle, speechOutput, "", true)
  );

}

/*
Called when the user specifies an intent for this skill.
*/
function onIntent(intentRequest, session, callback)
{
  /*console.log("onIntent requestId=" + intentRequest.requestId
    + ", sessionId=" + session.sessionId);*/

  var intent = intentRequest.intent,
    intentName = intentRequest.intent.name;

  // dispatch custom intents to handlers here
  if (intentName == 'TestIntent') {
    handleTestRequest(destinationInput, returnCallback, intent, session, callback);
    //console.log(JSON.stringify(intent));
    //console.log(JSON.stringify(session));
    //console.log(callback);
  } else {
    throw "Invalid intent";
  }

}

/*
Called when the user ends the session.
Is not called when the skill returns shouldEndSession=true.
*/
function onSessionEnded(sessionEndedRequest, session)
{
  /*console.log("onSessionEnded requestId=" + sessionEndedRequest.requestId
    + ", sessionId=" + session.sessionId);*/

  // Add any cleanup logic here

}

function handleTestRequest(destinationInput, returnCallback, intent, session, callback)
{
  if (moduleAvailable('unirest')) {
      /*console.log("unirest found");*/
  }
  unirest.post(destinationUrl)
  .headers(destinationHeaders)
  .send(destinationInput)
  .end(
    function (response)
    {
      //console.log("unirest '.end' function triggered");
      var returnResultsContent = JSON.parse(response.body);
      var results = "no results found";
      // If the json contains search results
      if (returnResultsContent.hasOwnProperty('resultsList'))
      {
        // Check to see if there are any search results found
        if (returnResultsContent.resultsList[0].hasOwnProperty('title'))
        {
          results = returnResultsContent.resultsList[0].title + '\n' + returnResultsContent.resultsList[0].snippet;
        }
      }
      // If the json contains a question
      if (returnResultsContent.hasOwnProperty('question'))
      {
        results = 'Question Title \n' + returnResultsContent.question.title + '\n' + 'Question Body \n' + returnResultsContent.question.body;
        /*
        * This is how one would strip out HTML but I don't know if I should.*/
        resultsWithHtml = 'Question Title \n\n' + returnResultsContent.question.title + '\n\n' + 'Question Body \n\n' + returnResultsContent.question.body;
        results = resultsWithHtml.replace(/(<([^>]+)>)/ig, "");
      }
      // If the json contains an article
      if (returnResultsContent.hasOwnProperty('article'))
      {
        results = returnResultsContent.article.title + '\n' + returnResultsContent.article.body;
      }
      returnCallback(results);
      var repromptText = "";
      var shouldEndSession = true;
      // The magic happens here. Note the callback.
      callback(session.attributes, buildSpeechletResponseWithoutCard(results, repromptText, shouldEndSession));
    }
  );
  /*console.log("returnSearchResults finished, awaiting results from unirest.post.");*/
}

/*
Helper functions to build responses
*/
function buildSpeechletResponse(title, output, repromptText, shouldEndSession)
{
  return {
    outputSpeech:
    {
      type: "PlainText",
      text: output
    },
    card:
    {
      type: "Simple",
      title: title,
      content: output
    },
    reprompt:
    {
      outputSpeech: {
        type: "PlainText",
        text: repromptText
      }
    },
    shouldEndSession: shouldEndSession
  };

}

function buildSpeechletResponseWithoutCard(results, repromptText, shouldEndSession) {
  var finalResults = {
    outputSpeech:
    {
      type: "PlainText",
      text: results
    },
    reprompt:
    {
        outputSpeech:
        {
          type: "PlainText",
          text: repromptText
        }
    },
    shouldEndSession: shouldEndSession
  };
  //console.log(finalResults);
  return finalResults;
}

function buildResponse(sessionAttributes, speechletResponse)
{
  return {
    version: "1.0",
    sessionAttributes: sessionAttributes,
    response: speechletResponse
  };
}
