
// unirest is a lightweight http request library
// http://unirest.io/nodejs.html
var unirest = require('unirest');
// http://stackoverflow.com/a/33067955
function moduleAvailable(unirest) {
    try {
        require.resolve(unirest);
        return true;
    } catch(e){}
    return false;
}
var destinationUrl = 'http://www.experts-exchange.com/jsp/mobileAPI.jsp';
var destinationHeaders =
{
  'Accept': 'vnd.experts-exchange.api+json;version=X',
  'x-ee-cmssiteid': '2',
  /* !IMPORTANT TODO: REMOVE THIS EE-SLI KEY */
  'x-ee-sli': '4825914:6ba3ec36ceff0931bf4631017792555822910172',
  'User-Agent': 'eeAlexa'
};
var destinationInput =
{
  'apiFunc': 'Article:get',
  'articleID': '19121',
  'allComments': 'false'
};

var returnCallback = function(results)
{
  return results;
};

function handleTestRequest(destinationInput, returnCallback)
{
  if (moduleAvailable('unirest')) {
      console.log("unirest found");
  }
  unirest.post(destinationUrl)
  .headers(destinationHeaders)
  .send(destinationInput)
  .end(
    function (response)
    {
      console.log("unirest '.end' function triggered");
      var returnResultsContent = JSON.parse(response.body);
      var results = "no results found";
      // If the json contains an article
      if (returnResultsContent.hasOwnProperty('article'))
      {
          results = returnResultsContent.article.title + '\n' + returnResultsContent.article.body;
      }
      returnCallback(results);
      buildSpeechletResponseWithoutCard(results);
    }
  );
  console.log("returnSearchResults finished, awaiting results from unirest.post.");
};










function buildSpeechletResponseWithoutCard(results) {
  var returnThisThing = {
    outputSpeech:
    {
      type: "PlainText",
      text: results
    },
    reprompt:
    {
        outputSpeech:
        {
          type: "PlainText",
          text: "If you needed a reprompt this is what it would say"
        }
    },
    shouldEndSession: true
  };
  console.log(returnThisThing);
  return returnThisThing;
};





handleTestRequest(destinationInput, returnCallback);
